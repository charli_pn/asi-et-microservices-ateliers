package com.sp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardGame {
	
	public static void main(String[] args) {
		SpringApplication.run(CardGame.class,args);
	}
}
