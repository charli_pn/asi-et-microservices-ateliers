package com.sp.repository;

import com.sp.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {

    List<User> findBySurname(String surname);

    User findBySurnameAndPassword(String surname, String password);
}
