package com.sp.repository;

import com.sp.model.AcquiredCard;
import com.sp.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AcquiredCardRepository extends CrudRepository<AcquiredCard, Integer> {

    List<AcquiredCard> findAllByIdUser(User user);

}
