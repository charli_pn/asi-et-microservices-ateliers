package com.sp.model;

import javax.persistence.*;

@Entity
public class AcquiredCard {

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @JoinColumn(name = "idCard", referencedColumnName = "id", nullable = false)
    private Card idCard;

    @ManyToOne
    @JoinColumn(name = "idUser", referencedColumnName = "id")
    private User idUser;
    private int hp;

    public AcquiredCard(){}
    public AcquiredCard(Card idCard, User idUser, int hp) {
        this.idCard = idCard;
        this.idUser = idUser;
        this.hp = hp;
    }

    @Override
    public String toString() {
        return "AcquiredCard{" +
                "id=" + id +
                ", idCard=" + idCard +
                ", idUser=" + idUser +
                ", hp=" + hp +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Card getIdCard() {
        return idCard;
    }

    public void setIdCard(Card idCard) {
        this.idCard = idCard;
    }

    public User getIdUser() {
        return idUser;
    }

    public void setIdUser(User idUser) {
        this.idUser = idUser;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }
}
