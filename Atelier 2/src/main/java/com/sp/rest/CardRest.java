package com.sp.rest;

import com.sp.model.AcquiredCard;
import com.sp.model.Card;
import com.sp.model.User;
import com.sp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.sp.service.CardService;

import java.util.List;

@RestController
public class CardRest {
	@Autowired
	CardService cardService;

	@Autowired
	UserService userService;

	@RequestMapping(method = RequestMethod.GET, value = "/cards")
	public ResponseEntity<List<Card>> getAllCards() {
		List<Card> cards = cardService.allCards();
		if (cards.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(cards, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/cards/{idUser}")
	public ResponseEntity<List<AcquiredCard>> getCardsByUser(@PathVariable int idUser) {
		User user = userService.findById(idUser);
		List<AcquiredCard> cards = cardService.getCardsByUser(user);
		if (cards.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(cards, HttpStatus.OK);
	}

	@RequestMapping(method= RequestMethod.POST,value="/card/add")
	public void addCard(@RequestBody Card card) {
		cardService.addCard(card);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/card/buy/{idCard}",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buyCard(@PathVariable int idCard, @RequestParam int idUser) {
		User user = userService.findById(idUser);
		Card card = cardService.findById(idCard);
		if (card == null) {
			return new ResponseEntity<>("La carte n'existe pas", HttpStatus.CONFLICT);
		}else if (user == null){
			return new ResponseEntity<>("L'utilisateur n'existe pas", HttpStatus.CONFLICT);
		} else if (user.getMoney() - card.getPrice() < 0) {
			return new ResponseEntity<>("Vous n'avez pas assez d'argent.", HttpStatus.CONFLICT);
		} else {
			AcquiredCard achat = cardService.userBuyCard(card, user);
			return new ResponseEntity<>(achat, HttpStatus.OK);
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/card/sell")
	public ResponseEntity<?> sellCard(@RequestBody AcquiredCard acquiredCard) {
		if (!cardService.acquiredCardExist(acquiredCard)) {
			return new ResponseEntity<>("La carte n'existe pas", HttpStatus.CONFLICT);
		}else if (acquiredCard.getIdUser() == null){
			return new ResponseEntity<>("La carte ne vous appartient pas", HttpStatus.CONFLICT);
		} else {
			cardService.userSellCard(acquiredCard);
			return new ResponseEntity<>("Vente effectuée", HttpStatus.OK);
		}
	}
}
