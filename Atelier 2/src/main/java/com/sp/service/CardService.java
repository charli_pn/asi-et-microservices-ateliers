package com.sp.service;

import com.sp.model.AcquiredCard;
import com.sp.model.User;
import com.sp.repository.AcquiredCardRepository;
import com.sp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.model.Card;
import com.sp.repository.CardRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CardService {
	private final CardRepository cardRepository;
	private final AcquiredCardRepository acquiredCardRepository;
	private final UserRepository userRepository;

	@Autowired
	public CardService(CardRepository cardRepository, AcquiredCardRepository acquiredCardRepository, UserRepository userRepository) {
		this.cardRepository = cardRepository;
		this.acquiredCardRepository = acquiredCardRepository;
		this.userRepository = userRepository;
	}


	public void addCard(Card c) {
		Card createdCard = cardRepository.save(c);
		System.out.println(createdCard);
	}

	public AcquiredCard userBuyCard(Card card, User user) {
		AcquiredCard achat = new AcquiredCard(card,user,card.getHp());
		acquiredCardRepository.save(achat);
		user.setMoney(user.getMoney() - card.getPrice());
		userRepository.save(user);
		System.out.println(achat);
		return achat;
	}

	public void userSellCard(AcquiredCard acquiredCard) {
		User user = acquiredCard.getIdUser();
		acquiredCard.setIdUser(null);
		user.setMoney(user.getMoney() + acquiredCard.getIdCard().getPrice());
		userRepository.save(user);
		acquiredCardRepository.save(acquiredCard);
	}

	public boolean cardExist(Card card) {
		return cardRepository.findById(card.getId()).isPresent();
	}

	public boolean acquiredCardExist(AcquiredCard acquiredCard) {
		return acquiredCardRepository.findById(acquiredCard.getId()).isPresent();
	}

	public Card findById(int idCard) {
		return cardRepository.findById(idCard).orElse(null);
	}

	public List<Card> allCards() {
		Iterable<Card> cards = cardRepository.findAll();
		List<Card> result = new ArrayList<>();
		cards.forEach(result::add);
		return result;
	}

	public List<AcquiredCard> getCardsByUser(User user) {
		return acquiredCardRepository.findAllByIdUser(user);
	}
}
