package com.cardgame.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cardgame.controler.CardDao;
import com.cardgame.model.CardModel;

/**
 * Servlet implementation class CarteServlet
 */
@WebServlet("/CarteServlet")
public class CarteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String CARDLIST = "card_list";
	private CardDao dao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CarteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getDao();
		List<CardModel> cardList = this.dao.getCards();
		
		request.getSession().setAttribute(CARDLIST, cardList);
		
        this.getServletContext().getRequestDispatcher( "/WEB-INF/card.jsp" ).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getDao();
		try {
			CardModel card = new CardModel(
					request.getParameter("card_name"),
					request.getParameter("card_desc"),
					request.getParameter("card_family"),
					Integer.parseInt(request.getParameter("card_hp")),
					Integer.parseInt(request.getParameter("card_energy")),
					Integer.parseInt(request.getParameter("card_defense")),
					Integer.parseInt(request.getParameter("card_attack")),
					request.getParameter("card_imgUrl")
				);
			
			this.dao.addCard(card);;
			List<CardModel> cardList = this.dao.getCards();
			
			request.getSession().setAttribute(CARDLIST, cardList);
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/card.jsp").forward(request, response);

		} catch(Exception e) {
			// DO NOTHING, FORM IS INVALID
		}
	}
	
	public void getDao(){
        if(this.getServletContext().getAttribute("DAO")!=null){
            this.dao=(CardDao) this.getServletContext().getAttribute("DAO");
        }else{
            this.dao=new CardDao();
            this.getServletContext().setAttribute("DAO",this.dao);
        }
    }


}
