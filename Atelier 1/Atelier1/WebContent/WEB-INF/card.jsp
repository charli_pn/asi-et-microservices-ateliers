<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" >
<title>List of Cards</title>
</head>
<body>
	<c:forEach items="${sessionScope.card_list}" var="card">
		<h1>Card name : ${card.name}</h1>
		<h2>Card description: ${card.description}</h2>
		<h2>Card family: ${card.family}</h2>
		<hr>
	</c:forEach>
</body>
</html>