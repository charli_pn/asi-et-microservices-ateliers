<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add a card</title>
</head>
<body>
<h1>Add a card</h1>
<form action="/JSP-Servlet-JavaBean-Sample/cards" method="post">
    <label for="card_name">Card name</label>
    <input type="text" name="card_name" id="card_name" />
    <label for="card_desc">Card description</label>
    <input type="text" name="card_desc" id="card_desc" />
    <label for="card_family">Card family</label>
    <input type="text" name="card_family" id="card_family" />
    <label for="card_hp">Card HP</label>
    <input type="text" name="card_hp" id="card_hp" />
    <label for="card_energy">Card energy</label>
    <input type="text" name="card_energy" id="card_energy" />
    <label for="card_defense">Card defense</label>
    <input type="text" name="card_defense" id="card_defence" />
    <label for="card_attack">Card attack</label>
    <input type="text" name="card_attack" id="card_attack" />
    <label for="card_imgUrl">Card URL</label>
    <input type="text" name="card_imgUrl" id="card_imgUrl" />
    <input type="submit" value="Valider" />
</form>
</body>
</html>