$(document).ready(function() {
        // Lien vers le WebService pour récupérer une card
        var pathname = '/WebServiceCard/rest/servicescard/find?name=';
        // Récupérations des liens de l'URL
        const urlParams = new URLSearchParams(window.location.search);
        const id = urlParams.get('search');
        // Requête AJAX de type GET permettant de charger une carte au chargement de la page si le paramètre search est renseigné
        $.ajax({
                url: pathname + id,
                type:'GET',
                dataType:'json',
                success: function(data,status){
                        $('#cardFamilyImgId')[0].src         = data.imageUrl;
                        $('#cardFamilyNameId')[0].innerText  = data.family;
                        $('#cardImgId')[0].src               = data.imgUrl;
                        $('#cardNameId')[0].innerText        = data.name;
                        $('#cardDescriptionId')[0].innerText = data.description;
                        $('#cardHPId')[0].innerText          = data.hp;
                        $('#cardEnergyId')[0].innerText      = data.energy;
                        $('#cardAttackId')[0].innerText      = data.attack;
                        $('#cardDefenceId')[0].innerText     = data.defence;
                        
                }
        });
});

$("#myForm").submit(function (e) {
    e.preventDefault();
    var pathname = '/WebServiceCard/rest/servicescard/find?name=';
    $.ajax({
            url: pathname + $("#search").val(),
            type:'GET',
            dataType:'json',
            success: function(data,status){
                    $('#cardFamilyImgId')[0].src         = data.imageUrl;
                    $('#cardFamilyNameId')[0].innerText  = data.family;
                    $('#cardImgId')[0].src               = data.imgUrl;
                    $('#cardNameId')[0].innerText        = data.name;
                    $('#cardDescriptionId')[0].innerText = data.description;
                    $('#cardHPId')[0].innerText          = data.hp;
                    $('#cardEnergyId')[0].innerText      = data.energy;
                    $('#cardAttackId')[0].innerText      = data.attack;
                    $('#cardDefenceId')[0].innerText     = data.defence;
                    
            }
    })
});
